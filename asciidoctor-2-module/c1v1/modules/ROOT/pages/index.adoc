= {page-component-title}
:page-relative-src-path: {page-relative}

== Page Coordinates

page-component-name: {page-component-name}

page-component-version: {page-component-version}

page-module: {page-module}

page-relative-src-path: {page-relative-src-path}


== A page

And some content.

[source,js]
----
class HighlightSyntaxHighlighter extends SyntaxHighlighterBase {

  handlesHighlighting () { return true } //<1>

  highlight (node, source, lang, opts) {
    console.log('source', source)
    const result = hljs.highlight(lang, source).value
    console.log('result:', result)
    return result
  }
}

SyntaxHighlighter.register('highlight.js', HighlightSyntaxHighlighter)
----
<1> Conum target.
