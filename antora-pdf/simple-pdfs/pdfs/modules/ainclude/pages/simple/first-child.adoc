= This is a first child page for the simple ainclude example

Here is some content, linking to the first section of the second page: xref:simple/second-child.adoc[].

== This is the second section of the first child page

Here is some content, linking to the second section of the second child page: xref:simple/second-child.adoc#_this_is_the_second_section_of_the_second_child_page[].

Here is a link to the custom content on the second page: xref:simple/second-child.adoc#_custom_id_2[].

Here is a link to the custom content on this page: xref:#_custom_id[]

'''
[[_custom_id,Custom Id]]
Here is a paragraph with a custom Id.
