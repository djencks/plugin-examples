= Simple usage of `ainclude` and `listToAinclude`

The same content is included via `ainclude` and `listToAinclude`

== ainclude

link:{attachmentsdir}/ainclude-pdf.pdf[]

== listToAinclude

link:{attachmentsdir}/listToAinclude-pdf.pdf[]

