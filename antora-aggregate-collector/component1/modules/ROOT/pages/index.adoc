= {page-component-title}

== Page Coordinates

page-component-name: {page-component-name}

page-component-version: {page-component-version}

page-module: {page-module}

page-relative: {page-relative}


== A page

The pages matching `*.adoc` are collected from `antora-aggregate-collector/pa?/src/main/docs` into the ROOT module.
