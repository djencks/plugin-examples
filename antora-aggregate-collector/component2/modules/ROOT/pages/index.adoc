= {page-component-title}

== Page Coordinates

page-component-name: {page-component-name}

page-component-version: {page-component-version}

page-module: {page-module}

page-relative: {page-relative}


== A page

The pages `*/README.adoc` are collected from directories in this component matching `pb?/src/main/docs` and deindexfied into the ROOT module.
For instance, `pageb1/README.adoc` becomes `pageb1.adoc`.
